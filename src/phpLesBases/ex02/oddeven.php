<?php

echo 'Entrez un nombre: ';
while (true) {

    $chiffre = trim(fgets(STDIN));

    if (is_numeric($chiffre)) {
        if ($chiffre % 2 == 0) {
            echo "Le chiffre " . $chiffre . " est Pair\nEntrez un nombre: ";
        } else {
            echo "Le chiffre " . $chiffre . " est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'" . $chiffre . "' n'est pas un chiffre\nEntrez un nombre: ";
    }
}